#include "Flickable.h"
#include <QtQuick/QQuickWindow>

#include <QtQuick/private/qquickitem_p.h>

namespace QmlMyItems {

namespace {

void lift(QQuickFlickable * from, QQuickFlickable * to)
{
	Q_ASSERT(from && to);

	qDebug() << __PRETTY_FUNCTION__;
	to->flick(-from->horizontalVelocity(), -from->verticalVelocity());
	from->cancelFlick();
	from->setContentY(from->originY());
}

}

void Flickable::import(const char * moduleName)
{
	qmlRegisterType<Flickable>(moduleName, 1, 0, "Flickable");
}

Flickable::Flickable(QQuickItem * parentItem)
	: QQuickFlickable(parentItem)
	, headerContent_(new QQuickItem(this))
	, bodyContent_(new QQuickItem(this))
{
}

void Flickable::componentComplete()
{
	QQuickFlickable::componentComplete();
	headerContent_->setZ(z() + 2); // TODO
	bodyContent_->setParentItem(contentItem());
	lastContentY_ = contentY();

	updateContentGeometry();
	updateHeaderGeometry();
	updateBodyGeometry();

	connect(this, &Flickable::widthChanged, this, &Flickable::onWidthChanged);
	connect(this, &Flickable::heightChanged, this, &Flickable::onHeightChanged);

	connect(this, &Flickable::headerMaxHeightChanged, this, &Flickable::onHeaderMaxHeightChanged);
	connect(this, &Flickable::headerMinHeightChanged, this, &Flickable::updateHeaderGeometry);

	connect(this, &Flickable::contentYChanged, this, &Flickable::onContentYChanged);
}

void Flickable::setNestedFlickable(QQuickFlickable * flick)
{
	qDebug() << __PRETTY_FUNCTION__ << flick;

	if (flick == flick_ || flick == this) return;

	if (flick_) {
		disconnect(flick_, &QQuickFlickable::contentYChanged, this, &Flickable::onFlickContentYChanged);
		flick_->removeEventFilter(this);
		flick_->setRebound(nullptr);
	}

	flick_ = flick;

	if (flick_) {
		flick_->installEventFilter(this);
		connect(flick_, &QQuickFlickable::contentYChanged, this, &Flickable::onFlickContentYChanged);
		flick_->setRebound(rebound());
	}
}

void Flickable::setHeader(QQuickItem * item)
{
	auto currentHeader = header();

	if (currentHeader == item || item == this) return;

	if (currentHeader) {
		currentHeader->setParentItem(nullptr);
	}
	if (item) {
		item->setParentItem(headerContent_);
	}
	emit headerChanged();
}

QQmlListProperty<QQuickItem> Flickable::body()
{
	return QQuickItemPrivate::get(bodyContent_)->children();
}

bool Flickable::eventFilter(QObject * watched, QEvent * event)
{
//	qDebug() << __PRETTY_FUNCTION__ << watched << event;

	if (watched != flick_) {
		return QQuickFlickable::eventFilter(watched, event);
	}

	switch (event->type()) {
		case QEvent::Wheel: {
			// TODO generalize
			if (mustGrab()) {
				qDebug() << "Grab" << normalizedContentY() << verticalVelocity();
				wheelEvent(static_cast<QWheelEvent *>(event));
				return true; // do not propogate to child
			}
			break;
		}

		default:
			break;
	}

	return QQuickFlickable::eventFilter(watched, event);
}

bool Flickable::childMouseEventFilter(QQuickItem * item, QEvent * event)
{
	if (flick_ != item) {
		return QQuickFlickable::childMouseEventFilter(item, event);
	}

	if (flick_->isDragging() && mustGrab()) {
		qDebug() << __PRETTY_FUNCTION__ << item << event << flick_->isDragging() << mustGrab();
	}

	switch(event->type()) {
		case QEvent::MouseMove: {
			if (flick_->isDragging() && mustGrab()) {
				qDebug() << "Grab" << normalizedContentY() << verticalVelocity();

				auto me = static_cast<QMouseEvent *>(event);
				const auto pos = contentItem()->mapFromScene(me->windowPos());
				sme_ = QSharedPointer<QMouseEvent>::create(QMouseEvent::MouseButtonPress,
										   pos, //pos, pos,
										   me->button(),
										   me->buttons(),
										   me->modifiers()//,
//										   Qt::MouseEventSynthesizedByApplication
										   );

//				auto sme = new QMouseEvent(QMouseEvent::MouseButtonRelease,
//										   pos, //pos, pos,
//										   me->button(),
//										   me->buttons(),
//										   me->modifiers()//,
////										   Qt::MouseEventSynthesizedByApplication
//										   );

//				QCoreApplication::postEvent(window(), sme_.data());

				grabbedItem_ = flick_;
				grabMouse();

				return true; // do not propogate to child
			}
			break;
		}

		case QEvent::UngrabMouse:
			if (sme_) {
				QCoreApplication::sendEvent(window(), sme_.data());
				sme_.reset();
			}
//				me->accept();
//				return true;

		default:
			break;
	}

	return QQuickFlickable::childMouseEventFilter(item, event);
}

void Flickable::mousePressEvent(QMouseEvent * e)
{
	qDebug() << __PRETTY_FUNCTION__ << e << normalizedContentY();
	QQuickFlickable::mousePressEvent(e);
}

void Flickable::mouseReleaseEvent(QMouseEvent * e)
{
	qDebug() << __PRETTY_FUNCTION__ << e << verticalVelocity();
	grabbedItem_ = nullptr;
	QQuickFlickable::mouseReleaseEvent(e);
}

void Flickable::mouseMoveEvent(QMouseEvent * e)
{
	qDebug() << __PRETTY_FUNCTION__ << e << verticalVelocity() << normalizedContentY();
	QQuickFlickable::mouseMoveEvent(e);

	if (flick_ && normalizedContentY() >= 0 && contentYDelta_ > 0 && !flick_->isAtYEnd()) {
		qDebug() << "Ungrab" << normalizedContentY();

		flick_->grabMouse();
		grabbedItem_ = nullptr;

		cancelFlick();
		setContentY(originY());

		auto me = new QMouseEvent(QMouseEvent::MouseButtonPress, e->localPos(), e->button(), e->buttons(), e->modifiers());
		QCoreApplication::postEvent(window(), me); // post on next event loop
	}
}

void Flickable::updateContentGeometry()
{
	qDebug() << __PRETTY_FUNCTION__ << width() << height();
	setContentHeight(height() + originY());
	setContentWidth(width());
}

void Flickable::updateBodyGeometry()
{
	bodyContent_->setY(headerMaxHeight_);
	bodyContent_->setHeight(height() - headerMinHeight_);
	bodyContent_->setWidth(width());
}

void Flickable::updateHeaderGeometry()
{
	auto oldHeaderScale = headerScale();

	headerContent_->setHeight(headerMaxHeight_ - contentY());
	headerContent_->setWidth(width());

	if (oldHeaderScale != headerScale()) {
		emit headerScaleChanged();
	}
}

void Flickable::onWidthChanged()
{
	updateContentGeometry();
	updateHeaderGeometry();
	updateBodyGeometry();
}

void Flickable::onHeightChanged()
{
	updateContentGeometry();
	updateBodyGeometry();
}

void Flickable::onHeaderMaxHeightChanged()
{
	updateContentGeometry();
	updateHeaderGeometry();
	updateBodyGeometry();
}

void Flickable::onFlickContentYChanged()
{
	// доводка: мотнули child вниз и отпустили мышу
	if (flick_ && flick_->verticalVelocity() < 0 && flick_->isAtYBeginning() && !flick_->isDragging()) {
		lift(flick_.data(), this);
	}
}

void Flickable::onContentYChanged()
{
	// доводка: мотнули parent вверх и отпустили мышу
	if (flick_ && normalizedContentY() >= 0 && flick_->isAtYBeginning() && verticalVelocity() > 0 && !isDragging()) {
		qDebug() << __FUNCTION__ << normalizedContentY();

//		updateBodyGeometry();
		lift(this, flick_.data());
	}

	updateHeaderGeometry();

	contentYDelta_ = contentY() - lastContentY_;
	lastContentY_ = contentY();
}

bool Flickable::mustGrab() const
{
	return
			// если header не скрыт (headerMinSize), то все жесты перенаправляем в parent flickable
			(normalizedContentY() < 0 && (flick_->isAtYBeginning() || flick_->verticalVelocity() > 0)) ||
			// если header скрыт и
			// child flickable проскроллен до начала, и мы тянем вниз,
			// то parent flickable захватывает жест, чтобы развернуть хидер
			(flick_->verticalVelocity() < 0 && flick_->isAtYBeginning());
}

}
