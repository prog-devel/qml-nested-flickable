#pragma once

#include <QtQuick/QQuickItem>
#include <QtQml/QQmlParserStatus>
#include <QtCore/QPointer>
#include <QtCore/QSharedPointer>
#include <QtGui/QMouseEvent>

#include <QtQuick/private/qquickflickable_p.h>

namespace QmlMyItems {

class Flickable
		: public QQuickFlickable
{
	Q_OBJECT

	Q_PROPERTY(QQuickFlickable * nestedFlickable READ nestedFlickable WRITE setNestedFlickable NOTIFY nestedFlickableChanged)
	Q_PROPERTY(QQuickItem * header READ header WRITE setHeader NOTIFY headerChanged)
	Q_PROPERTY(qreal headerScale READ headerScale NOTIFY headerScaleChanged)
	Q_PROPERTY(QQmlListProperty<QQuickItem> body READ body)

	Q_PROPERTY(qreal headerMinHeight MEMBER headerMinHeight_ NOTIFY headerMinHeightChanged)
	Q_PROPERTY(qreal headerMaxHeight MEMBER headerMaxHeight_ NOTIFY headerMaxHeightChanged)

	Q_CLASSINFO("DefaultProperty", "body")

public:
	static void import(const char * moduleName);
	Flickable(QQuickItem * parentItem = nullptr);

	QQuickFlickable * nestedFlickable() const { return flick_; }

	QQuickItem * header() const { return !headerContent_->childItems().isEmpty() ? headerContent_->childItems()[0] : nullptr; }
	void setHeader(QQuickItem * item);
	qreal headerScale() const { return (headerContent_->height() - headerMinHeight_) / (headerMaxHeight_ - headerMinHeight_); }

	QQmlListProperty<QQuickItem> body();

	qreal originY() const override { return headerMaxHeight_ - headerMinHeight_; }
	qreal normalizedContentY() const { return contentY() - originY(); }

signals:
	void nestedFlickableChanged();
	void headerChanged();
	void headerMinHeightChanged();
	void headerMaxHeightChanged();
	void headerScaleChanged();
	void bodyChanged();

protected: // QObject
	bool eventFilter(QObject *, QEvent *) override;

protected: // QQmlParserStatus
	void componentComplete() override;

protected: // QQuickItem
	bool childMouseEventFilter(QQuickItem *, QEvent *) override;

	void mousePressEvent(QMouseEvent *) override;
	void mouseReleaseEvent(QMouseEvent *) override;
	void mouseMoveEvent(QMouseEvent *) override;

protected: // own methods
	void setNestedFlickable(QQuickFlickable * flick);

protected slots:
	void updateContentGeometry();
	void updateBodyGeometry();
	void updateHeaderGeometry();

	void onWidthChanged();
	void onHeightChanged();

	void onHeaderMaxHeightChanged();

	void onContentYChanged();
	void onFlickContentYChanged();

private:
	bool mustGrab() const;

private:
	QPointer<QQuickFlickable> grabbedItem_;
	QPointer<QQuickFlickable> flick_;
	QQuickItem * headerContent_{nullptr};
	QQuickItem * bodyContent_{nullptr};

	qreal headerMinHeight_{0};
	qreal headerMaxHeight_{0};

	qreal lastContentY_{0};
	qreal contentYDelta_{0};

	QSharedPointer<QMouseEvent> sme_{nullptr};
};

}
