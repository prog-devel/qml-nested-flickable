#include "SwipeFrame.h"

#include <vector>

namespace QmlMyItems {

void TabsModel::import(const char * moduleName)
{
	qmlRegisterType<SwipeFrame>(moduleName, 1, 0, "SwipeFrame");
	qmlRegisterType<TabsModel>(moduleName, 1, 0, "TabsModel");
	qmlRegisterType<Tab>(moduleName, 1, 0, "Tab");
}

Tab::Tab(QObject * parent)
	: QObject(parent)
{

}

TabsModel::TabsModel(QObject * parent)
	: QAbstractListModel(parent)
{
}

QHash<int, QByteArray> TabsModel::roleNames() const
{
	return {
		{RoleTabName, QByteArrayLiteral("tabName")},
		{RoleDelegate, QByteArrayLiteral("delegate")}
	};
}

int TabsModel::rowCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);
	return count();
}

QVariant TabsModel::data(const QModelIndex & index, int role) const
{
	if (!index.isValid()) return {};
	const int idx = index.row();
	if (idx < 0 || idx >= count()) return {};

	const Tab * tab = tabs_.at(idx);

	switch (role) {
		case RoleTabName:
			return tab->title;
		case RoleDelegate:
			return QVariant::fromValue<QQmlComponent *>(tab->delegate);
		default:
			break;
	}

	return {};
}

bool TabsModel::setData(const QModelIndex & index, const QVariant &value, int role)
{
	if (!index.isValid()) return {};
	const int idx = index.row();
	if (idx < 0 || idx >= count()) return {};

	Tab * tab = tabs_.at(idx);

	switch (role) {
		case RoleTabName:
			tab->title = value.toString();
			break;
		case RoleDelegate:
			tab->delegate = value.value<QQmlComponent *>();
			break;
		default:
			return false;
	}

	emit dataChanged(index, index, {role});
	return true;
}

void TabsModel::clear()
{
	beginResetModel();
	tabs_.clear();
	endResetModel();
}

void TabsModel::append(Tab * item)
{
	qDebug() << __PRETTY_FUNCTION__ << this << item;

	int idx = tabs_.size();

	beginInsertRows(QModelIndex(), idx, idx);

	if (!item->parent()) {
		item->setParent(this);
	}

	tabs_.push_back(item);

	endInsertRows();
}


void SwipeFrame::tabs_add(Tabs * p, Tab * item)
{
	qDebug() << __PRETTY_FUNCTION__ << item;
	auto * model = static_cast<TabsModel *>(p->data);
	Q_ASSERT(model);

	model->append(item);
}

int SwipeFrame::tabs_count(Tabs * p)
{
	auto * model = static_cast<TabsModel *>(p->data);
	Q_ASSERT(model);
	return model->tabs().size();
}

Tab * SwipeFrame::tabs_at(Tabs * p, int idx)
{
	auto * model = static_cast<TabsModel *>(p->data);
	Q_ASSERT(model);
	if (idx < 0 || idx >= model->count()) return nullptr;
	return model->tabs().at(idx);
}

void SwipeFrame::tabs_clear(Tabs * p)
{
	auto * model = static_cast<TabsModel *>(p->data);
	Q_ASSERT(model);

	model->clear();
}


SwipeFrame::SwipeFrame(QQuickItem * parentItem)
	: QQuickListView(parentItem)
	, model_(new TabsModel(this))
{
	connect(this, &SwipeFrame::currentItemChanged, this, &SwipeFrame::currentTabChanged);
}

Tab	* SwipeFrame::currentTab() const
{
	return tab(currentIndex());
}

Tab * SwipeFrame::tab(int idx) const
{
	return idx >= 0 ? model_->tabs().at(idx) : nullptr;
}

void SwipeFrame::classBegin()
{
	QQuickListView::classBegin();

	setOrientation(Horizontal);
	setSnapMode(SnapOneItem);
	setHighlightRangeMode(StrictlyEnforceRange);

	setModel(QVariant::fromValue(model_));

}

SwipeFrame::Tabs SwipeFrame::tabs()
{
	return {this, model_,
		SwipeFrame::tabs_add,
		SwipeFrame::tabs_count,
		SwipeFrame::tabs_at,
		SwipeFrame::tabs_clear
	};
}

}

#include "moc_SwipeFrame.cpp"
