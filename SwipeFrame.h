#pragma once

#include <QtCore/QAbstractListModel>
#include <QtQuick/private/qquicklistview_p.h>

namespace QmlMyItems {

class Tab: public QObject
{
	Q_OBJECT

	Q_PROPERTY(QQmlComponent * delegate MEMBER delegate NOTIFY onDelegateChanged)
	Q_PROPERTY(QString title MEMBER title NOTIFY onTitleChanged)

public:
	Tab(QObject * parent = nullptr);

	QPointer<QQmlComponent> delegate;
	QString title;

signals:
	void onDelegateChanged();
	void onTitleChanged();
};

class TabsModel
		: public QAbstractListModel
{
public:
	enum Roles {
		RoleTabName = Qt::UserRole,
		RoleDelegate
	};

	using Tabs = std::vector<Tab *>;

	static void import(const char * moduleName);

	TabsModel(QObject * parent = nullptr);

	int count() const { return static_cast<int>(tabs_.size()); }
	Tabs tabs() const { return tabs_; }
	void clear();
	void append(Tab * item);

public: // QAbstractItemModel
	QHash<int, QByteArray> roleNames() const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role) override;

private:
	Tabs tabs_;
};

class SwipeFrame
		: public QQuickListView
{
	Q_OBJECT

	Q_PROPERTY(QQmlListProperty<QmlMyItems::Tab> tabs READ tabs)
	Q_PROPERTY(Tab * currentTab READ currentTab NOTIFY currentTabChanged)

	Q_CLASSINFO("DefaultProperty", "tabs")

public:
	using Tabs = QQmlListProperty<Tab>;

	SwipeFrame(QQuickItem * parentItem = nullptr);

	Tab	* currentTab() const;
	Q_INVOKABLE QmlMyItems::Tab * tab(int idx) const;

signals:
	void currentTabChanged();

protected:
	Tabs tabs();

protected:
	void classBegin() override;

private:
	static void tabs_add(Tabs *, Tab * item);
	static int tabs_count(Tabs *);
	static Tab * tabs_at(Tabs *, int idx);
	static void tabs_clear(Tabs *);

private:
	TabsModel * model_;
};

}
