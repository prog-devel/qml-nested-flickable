#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "Flickable.h"
#include "SwipeFrame.h"

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	QmlMyItems::Flickable::import("MyItems");
	QmlMyItems::TabsModel::import("MyItems");

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
