import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import MyItems 1.0 as My

Window {
    id: window
    visible: true
    width: 240
    height: 320
    visibility: Window.AutomaticVisibility
    title: qsTr("Nested flickable")

    property real scale: window.height / 320
    property real fontSize: 16

    function dip(val) {
        return val * scale;
    }

    Component {
        id: headerComp
        ColumnLayout {
            // TODO attached header options (isCollapsed)

            property real minHeight: movable.height

            spacing: 0
            anchors.fill: parent

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumHeight: 0

                clip: true

                Text {
                    anchors.fill: parent
                    // TODO attached hide factor
                    opacity: Math.min(showFactor, 1.0)
                    scale: showFactor
                    visible: showFactor > 0

                    text: qsTr("Resizable header")
                    font.pointSize: fontSize
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter

                    Rectangle {
                        anchors.fill: parent
                        z: parent.z - 1

                        opacity: 0.2
                        color: "red"
                    }
                }
            }

            PageIndicator {
                id: movable
                Layout.preferredHeight: dip(18)
                Layout.fillWidth: true
//                Layout.topMargin: dip(10)
//                Layout.bottomMargin: dip(10)

                interactive: true
                count: swipe.count
                currentIndex: swipe.currentIndex
                spacing: dip(10)

                onCurrentIndexChanged: {
                    swipe.currentIndex = currentIndex;
                }

                delegate: Text {
                    text: swipe.tab(index).title
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    font.pointSize: fontSize
                    color: movable.currentIndex === index ? "green" : "black"


                    Behavior on color {
                        ColorAnimation {
                            duration: 150
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: swipe.currentIndex = index
                    }

                    Rectangle {
//                        readonly property var currentItem: row.children.length ? row.children[movable.currentIndex] : null
//                        x: currentItem ? currentItem.x : 0
//                        width: currentItem ? currentItem.width : 0
                        width: parent.width
                        height: dip(2)
                        anchors.top: parent.bottom
                        opacity: 0.5
                        color: "green"
                        visible: index === movable.currentIndex

//                        Behavior on x {
//                            PropertyAnimation {
//                                duration: 150
//                            }
//                        }

//                        Behavior on width {
//                            PropertyAnimation {
//                                duration: 150
//                            }
//                        }
                    }
                }

                contentItem: Rectangle {
                    anchors.fill: movable
//                    color: "green"
//                    opacity: 0.2

                    Row {
                        id: row
                        anchors.centerIn: parent
                        anchors.margins: dip(4)
                        spacing: movable.spacing

                        Repeater {
                            id: repeater
                            height: childrenRect.height
                            width: parent.width
                            model: swipe.model
                            // FIXME: bull shit
                            delegate: movable.delegate
                        }
                    }
                }
            }
        }
    }

    My.Flickable {
        id: flick
        anchors.fill: parent

        rebound: Transition {
            NumberAnimation {
                properties: "x,y"
                duration: 1000
                easing.type: Easing.OutBounce
            }
        }

        headerMinHeight: header.item ? header.item.minHeight : 0
        headerMaxHeight: dip(100)

        header: Loader {
            id: header

            property real showFactor: flick.headerScale
            property var swipe: swipeFlick

            anchors.fill: parent
            sourceComponent: headerComp
        }

        nestedFlickable: swipeFlick.currentItem && swipeFlick.currentItem.sourceComponent === verticalListComp ? swipeFlick.currentItem.item : null

        My.SwipeFrame {
            id: swipeFlick

            anchors.fill: parent

            highlightMoveVelocity: 1000

            onCurrentItemChanged: {
                console.log(swipeFlick, "Current item", currentItem)
            }

            delegate: Loader {
                width: swipeFlick.width
                height: parent.height

                sourceComponent: model.delegate
            }

            My.Tab {
                title: "Tab 1"
                delegate: verticalListComp
            }

            My.Tab {
                title: "Tab 2"
                delegate: imageComp
            }

            My.Tab {
                title: "Tab 3"
                delegate: verticalListComp
            }
        }

    }

    Component {
        id: verticalListComp

        ListView {
            model: 10
            anchors.fill: parent

            flickableDirection: Flickable.VerticalFlick

            delegate: listItemComp

        }
    }

    Component {
        id: listItemComp
        Rectangle {
            width: parent.width
            height: dip(50)
            opacity: 0.5
            color: Qt.rgba(Math.random(1.0), Math.random(1.0), Math.random(1.0), 1.0)

            Text {
                anchors.fill: parent
                anchors.leftMargin: dip(10)
                text: "#" + index
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.pointSize: fontSize
            }
        }
    }

    Component {
        id: imageComp

        Item {
            Image {
                source: "/images/fox.jpg"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
                anchors.margins: dip(5)
            }
        }
    }
}
